# ej2 syncfusion sample

This project contain https://ej2.syncfusion.com/ library Tree Grid Tables

# Getting started

1. Go to project folder and install dependencies:

```sh
npm install
```

2. Launch development server, and open `localhost:4200` in your browser:

```sh
npm start
```

# Project structure

```
dist/                        web app production build
docs/                        project docs and coding guides
e2e/                         end-to-end tests
src/                         project source code
|- app/                      app components
|  |- core/                  core module (singleton services and single-use components)
|  |- shared/                shared module  (common components, directives and pipes)
|  |- app.component.*        app root component (shell)
|  |- app.module.ts          app root module definition
|  |- app-routing.module.ts  app routes
|  +- ...                    additional modules and components
|- assets/                   app assets (images, fonts, sounds...)
|- environments/             values for various build environments
|- theme/                    app global scss variables and theme
|- translations/             translations files
|- index.html                html entry point
|- main.scss                 global style entry point
|- main.ts                   app entry point
|- polyfills.ts              polyfills needed by Angular
+- setup-jest.ts             unit tests entry point
reports/                     test and coverage reports
proxy.conf.js                backend proxy configuration
```

# Main tasks

Task automation is based on https://ej2.syncfusion.com/ Tree Grid

| Task        | Description                                        |
| ----------- | -------------------------------------------------- |
| `npm start` | Run development server on `http://localhost:4200/` |

# What's in the box

The app template is based on https://ej2.syncfusion.com/angular/demos/#/material/treegrid/default
Project have two template grids one is in home menu section and 2nd is about section

#### Tasks

- Context Menu 1 - Right-click or long-press any "Column Header", to pop-up these menu items:
- Freeze On/Off: Freeze all columns to the left of current column.
- Filter On/Off: Enable "Filter Bar" in Parent Hierarchy Mode.
- Multi-Sort On/Off: Enable Multi-Sort for all columns.
- Add, Del, Edit: Users can Add/Del/Edit columns, (also Drag-n-drop to Reorder & Resize columns).
- Style: Set the current column Data Type, Default Value, Alignment, Text-wrap.
- Context Menu 2 - Rright-click or long-press any "Row", to pop-up these menu items.
- Multi-Select On/Off: Enable users to multi-select rows on both PC and Mobile.
- Copy; Cut: Copy/Cut multi-selected rows
  - There is a drop down below with section ("Clipboard Copy/Paste Mode") select Mode from there by default parent is selected
  - Now, there are ways for copy and copy with header
    - 1. a.Note: Ctrl + C - Selected rows data without header.
         b.Ctrl + Shift + H - Selected rows data with header.
    - 2. click on toolbar copy or copy with header
- Add, Del, Edit: Users can Add/Del/"Dialog-Edit" rows in Row-Editing-Mode
- Multi select rows implemented
- search implemented based on drop down below tree grid select mode to search from, on/off search functionalty implemented but by default search will be On.

#### Tasks half done

- Drag-n-drop to Move multi-selected rows.
- Paste Sibling: Insert the copied/cut Rows as siblings of the current row. Paste Child: Insert the copied/cut Rows as children of the current row
- The rows being Copied/Cut shall be highlighted in color, but not removed yet, until user perform Paste
- Font-Size, Color not set yet
- syncfusion does not have mobile treegrid its only for data grid, scroller will appear for scroll
- Horizontal scroller have some issues
