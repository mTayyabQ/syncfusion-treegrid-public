export interface Adaapter<T> {
  adapt(item: T): T;
}
