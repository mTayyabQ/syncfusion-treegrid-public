export class GridModel {
  constructor(
    public taskID: number,
    public taskName: string,
    public startDate: Date,
    public endDate: Date,
    public progress: number,
    public duration: number,
    public priority: string,
    public approved: boolean,
    public isInExpandState: boolean,
    public subtasks: any[]
  ) {}

  static adapt(item: any): GridModel {
    // return item.rows;
    return item.rows.map((item: any) => {
      return new GridModel(
        item?.taskID,
        item?.taskName,
        item?.startDate,
        item?.endDate,
        item?.progress,
        item?.duration,
        item?.priority,
        item?.approved,
        item?.isInExpandState,
        item?.subtasks
      );
    });
  }
}

//{ columns: [{ field: 'taskName', direction: 'Ascending' }, { field: 'taskID', direction: 'Descending' },  { field: 'startDate', direction: 'Ascending' }]  }
export class sortTable {
  sortedColumn = {
    columns: [
      {
        field: 'taskName',
        direction: 'Ascending',
        isSortable: true,
      },
    ],
  };
  constructor() {}
}
