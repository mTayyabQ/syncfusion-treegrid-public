import { GridModel } from './../../Models/gridModel';
import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';

import { PageSettingsModel, SortSettingsModel } from '@syncfusion/ej2-treegrid';
/* imp for freeze column */
import { NumericTextBoxComponent } from '@syncfusion/ej2-angular-inputs';

/* imp for context menu */
import {
  ToolbarService,
  SortService,
  ResizeService,
  PageService,
  EditService,
  ExcelExportService,
  PdfExportService,
  ContextMenuService,
} from '@syncfusion/ej2-angular-treegrid';
import { EditSettingsModel } from '@syncfusion/ej2-treegrid';
/* filter on/of imp */
// import { ChangeEventArgs } from '@syncfusion/ej2-dropdowns'; //datagrid
// import { FilterService, GridComponent } from '@syncfusion/ej2-angular-grids';
import { TreeGridComponent as TreeGrid, FilterService } from '@syncfusion/ej2-angular-treegrid';

import { CheckBoxComponent } from '@syncfusion/ej2-angular-buttons';
import { DropDownListComponent, ChangeEventArgs } from '@syncfusion/ej2-angular-dropdowns';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { ClickEventArgs } from '@syncfusion/ej2-navigations';

@Component({
  selector: 'app-tree-grid',
  templateUrl: './tree-grid.component.html',
  styleUrls: ['./tree-grid.component.scss'],
  providers: [
    ToolbarService,
    FilterService,
    SortService,
    ResizeService,
    PageService,
    EditService,
    ExcelExportService,
    PdfExportService,
    ContextMenuService,
  ],
  encapsulation: ViewEncapsulation.None,
})
export class TreeGridComponent implements OnInit {
  pageSettings: PageSettingsModel = { pageSize: 10 }; // Default
  sortColumns = {};
  data: GridModel[] = [];
  headerDataKeys: string[] = [];
  @Input()
  set dataSource(dataSource: GridModel[]) {
    if (!dataSource) return;
    this.data = dataSource;
    this.headerDataKeys = Object.keys(dataSource[0]);
  }
  @Input() format: string = 'yMd';
  @Input() textAligntextAlign: string = 'Left'; //can be Right | Center
  @Input() sortSettings: SortSettingsModel = {};
  @Input() isSearchable: boolean = true;
  @Input() isSortable: boolean = true;
  @Input() childMapping: string = 'subtasks';
  @Input() isPaginated: boolean = true;
  @Input() treeColumnIndex: number = 1;
  @Input()
  set pageSize(pageSize: number) {
    this.pageSettings = { pageSize: pageSize };
  }
  /* context  */
  public contextMenuItems: string[] = [];
  public editing: EditSettingsModel;
  public editparams: Object;
  public toolbar: any[] = [];
  /* filter on/ of heirchy */
  public filterSettings: Object;
  public dropDownFilter: DropDownListComponent;
  public d1data: Object;
  public clipboardOptions: Object;

  public fields1: Object;
  /* CLIPBOARD */
  public selectionSettings: object;
  public alertHeader: string = 'Copy with Header';
  public hidden: Boolean = false;
  public target: string = '.control-section';
  public alertWidth: string = '300px';
  public alertContent: string = 'Atleast one row should be selected to copy with header';
  public showCloseIcon: Boolean = false;
  public animationSettings: Object = { effect: 'None' };
  @ViewChild('alertDialog')
  public alertDialog: DialogComponent;
  public alertDlgBtnClick = () => {
    this.alertDialog.hide();
  };
  public alertDlgButtons: Object[] = [
    { click: this.alertDlgBtnClick.bind(this), buttonModel: { content: 'OK', isPrimary: true } },
  ];

  constructor() {
    this.contextMenuItems = [
      'AutoFit',
      'AutoFitAll',
      'SortAscending',
      'SortDescending',
      'Edit',
      'Delete',
      'Save',
      'Cancel',
      'PdfExport',
      'ExcelExport',
      'CsvExport',
      'FirstPage',
      'PrevPage',
      'LastPage',
      'NextPage',
    ];
    this.editing = { allowDeleting: true, allowEditing: true, mode: 'Row' };
    this.pageSettings = { pageSize: 10 };
    this.editparams = { params: { format: 'n' } };
    /* filter parent heirchy */
    this.filterSettings = { type: 'FilterBar', hierarchyMode: 'Parent', mode: 'Immediate' };
    this.fields1 = { text: 'mode', value: 'id' };
    this.d1data = [
      { id: 'Parent', mode: 'Parent' },
      { id: 'Child', mode: 'Child' },
      { id: 'Both', mode: 'Both' },
      { id: 'None', mode: 'None' },
    ];

    /* clipboard */
    this.selectionSettings = { type: 'Multiple' };
    this.clipboardOptions = this.d1data;
    this.toolbar = [
      'ColumnChooser',
      { text: 'Copy', tooltipText: 'Copy', prefixIcon: 'e-copy', id: 'copy' },
      { text: 'Copy With Header', tooltipText: 'Copy With Header', prefixIcon: 'e-copy', id: 'copyHeader' },
    ];
  }

  ngOnInit() {}

  @ViewChild('treegrid')
  public treegrid: TreeGrid | any;
  @ViewChild('rows')
  public rows: any; //NumeranyicTextBoxComponent;
  @ViewChild('columns')
  public columns: any; //NumericTextBoxComponent;
  // public data: Object[] = [];
  columnValue: number = 1; //Browser['isDevice'] ? 1 : 2;
  /* filteron/of */
  // @ViewChild('dropdown1')
  // public dropdown1: DropDownListComponent;

  // After clicking 'Set' button, the `frozenRows` and `frozenColumns` values will be updated in Grid.
  btnClick() {
    this.treegrid.frozenRows = this.rows.value;
    this.treegrid.frozenColumns = this.columns.value;
  }

  /* filter on/of */
  public onChange(e: ChangeEventArgs): void {
    if (e.value === 'All') {
      this.treegrid.clearFiltering();
    } else {
      this.treegrid.filterByColumn('CategoryName', 'equal', e.value);
    }
  }

  change(e: ChangeEventArgs): void {
    let mode: any = <string>e.value;
    this.treegrid.filterSettings.hierarchyMode = mode;
    this.treegrid.clearFiltering();
    this.dropDownFilter.value = 'All';
  }
  /* End filter on/of */

  /* Multi sort on/ of */
  onMultiSort(columnName: any, $event: MouseEvent): void {
    if ($event) {
      this.treegrid.sortByColumn(columnName, 'Ascending', true);
    } else {
      this.treegrid.grid.removeSortColumn(columnName);
    }
  }

  /* clipboard */
  changeClipBoard(e: ChangeEventArgs): void {
    let mode: any = <string>e.value;
    this.treegrid.copyHierarchyMode = mode;
  }
  public toolbarClick(args: ClickEventArgs): void {
    if (this.treegrid.getSelectedRecords().length > 0) {
      let withHeader: boolean = false;
      if (args.item.id === 'copyHeader') {
        withHeader = true;
      }
      this.treegrid.copy(withHeader);
    } else {
      this.alertDialog.show();
    }
  }
}
