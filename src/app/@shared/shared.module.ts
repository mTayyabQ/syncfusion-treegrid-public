import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { FreezeService, TreeGridModule, TreeGridAllModule } from '@syncfusion/ej2-angular-treegrid';
import { DropDownListAllModule } from '@syncfusion/ej2-angular-dropdowns';

import { MaterialModule } from '@app/material.module';
import { LoaderComponent } from './loader/loader.component';
import { TreeGridComponent as TreeGrid } from './components/tree-grid/tree-grid.component';

/* freeze column */
import { NumericTextBoxAllModule } from '@syncfusion/ej2-angular-inputs';
/* filter on/ of */
import { GridAllModule } from '@syncfusion/ej2-angular-grids';
import { CheckBoxModule } from '@syncfusion/ej2-angular-buttons';
import { DatePickerModule, DatePickerAllModule } from '@syncfusion/ej2-angular-calendars';
/* clipboard */
import { DialogModule } from '@syncfusion/ej2-angular-popups';
@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    GridAllModule,
    TreeGridModule,
    TreeGridAllModule,
    DialogModule,
    CheckBoxModule,
    NumericTextBoxAllModule,
    DatePickerModule,
    DatePickerAllModule,
    TranslateModule,
    DropDownListAllModule,
  ],
  declarations: [TreeGrid, LoaderComponent],
  exports: [TreeGrid, LoaderComponent],
  providers: [FreezeService],
})
export class SharedModule {}
