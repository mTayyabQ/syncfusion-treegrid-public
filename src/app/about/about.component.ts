import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';

import { environment } from '@env/environment';
import { countries } from '@assets/jsontreegriddata';
import { ClickEventArgs } from '@syncfusion/ej2-navigations';

import { QueryCellInfoEventArgs } from '@syncfusion/ej2-grids';
import { addClass } from '@syncfusion/ej2-base';
import {
  TreeGridModule,
  SortService,
  FilterService,
  ReorderService,
  ITreeData,
  ToolbarService,
  ResizeService,
  PageService,
  EditService,
  ExcelExportService,
  PdfExportService,
  ContextMenuService,
  TreeGridComponent as TreeGrid,
} from '@syncfusion/ej2-angular-treegrid';

import { PageSettingsModel, SortSettingsModel } from '@syncfusion/ej2-treegrid';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  providers: [FilterService, SortService, ReorderService],
  encapsulation: ViewEncapsulation.None,
})
export class AboutComponent implements OnInit {
  public data: Object[] = [];
  public selectionSettings: object;
  public toolbar: any[] = [];
  public d1data: Object;
  public clipboardOptions: Object;

  public filterSettings: Object;

  @ViewChild('treegrid')
  public treegrid: TreeGrid | any;
  @ViewChild('alertDialog')
  public alertDialog: DialogComponent;
  public alertDlgBtnClick = () => {
    this.alertDialog.hide();
  };
  public alertDlgButtons: Object[] = [
    { click: this.alertDlgBtnClick.bind(this), buttonModel: { content: 'OK', isPrimary: true } },
  ];
  ngOnInit(): void {
    this.data = countries;
    this.filterSettings = { type: 'Excel' };

    /* clipboard */
    this.selectionSettings = { type: 'Multiple' };
    this.clipboardOptions = this.d1data;
    this.toolbar = [
      'ColumnChooser',
      { text: 'Copy', tooltipText: 'Copy', prefixIcon: 'e-copy', id: 'copy' },
      { text: 'Copy With Header', tooltipText: 'Copy With Header', prefixIcon: 'e-copy', id: 'copyHeader' },
    ];
  }

  queryCellInfo(args: QueryCellInfoEventArgs): void {
    if (args.column.field === 'gdp') {
      if (args.data[args.column.field] < 2) {
        args.cell.querySelector('.statustxt').classList.add('e-lowgdp');
        args.cell.querySelector('.statustemp').classList.add('e-lowgdp');
      }
    }
    if (args.column.field === 'unemployment') {
      if (args.data[args.column.field] <= 4) {
        addClass([args.cell.querySelector('.bar')], ['progressdisable']);
      }
      (args.cell.querySelector('.bar') as HTMLElement).style.width = args.data[args.column.field] * 10 + '%';
      args.cell.querySelector('.barlabel').textContent = args.data[args.column.field] + '%';
    }
    if (args.column.field === 'name') {
      let imgElement: HTMLElement = document.createElement('IMG');
      let val: string = !(<ITreeData>args.data).level
        ? args.data[args.column.field]
        : (<ITreeData>args.data).parentItem[args.column.field];
      imgElement.setAttribute('src', 'assets/treegrid/images/' + val + '.png');
      imgElement.classList.add('e-image');
      let div: HTMLElement = document.createElement('DIV');
      div.style.display = 'inline';
      div.appendChild(imgElement);
      let cellValue: HTMLElement = document.createElement('DIV');
      cellValue.innerHTML = args.cell.querySelector('.e-treecell').innerHTML;
      cellValue.setAttribute('style', 'display:inline;padding-left:6px');
      args.cell.querySelector('.e-treecell').innerHTML = '';
      args.cell.querySelector('.e-treecell').appendChild(div);
      args.cell.querySelector('.e-treecell').appendChild(cellValue);
    }
  }

  populationValue(field: string, data: Object): number {
    return data[field] / 1000000;
  }

  public toolbarClick(args: ClickEventArgs): void {
    if (this.treegrid.getSelectedRecords().length > 0) {
      let withHeader: boolean = false;
      if (args.item.id === 'copyHeader') {
        withHeader = true;
      }
      this.treegrid.copy(withHeader);
    } else {
      this.alertDialog.show();
    }
  }
}
