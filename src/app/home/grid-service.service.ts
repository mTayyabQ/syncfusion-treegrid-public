import { Injectable } from '@angular/core';
import { SortSettingsModel } from '@syncfusion/ej2-treegrid';

@Injectable({
  providedIn: 'root',
})
export class GridServiceService {
  public sortSettings: SortSettingsModel = {};
  constructor() {}

  initializeSorting(dataSource: any): void {
    let sortTableColumn: any = [];
    for (let key in dataSource[0]) {
      if (key === 'subtasks' || !key) continue;
      sortTableColumn.push({
        field: key,
        direction: 'Ascending',
      });
    }

    this.sortSettings = {
      columns: sortTableColumn,
    };
  }
}
