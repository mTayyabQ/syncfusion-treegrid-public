import { GridServiceService } from './grid-service.service';
import { Component, OnInit } from '@angular/core';
import { GridModel } from '@shared/Models/gridModel';

import { sampleData } from '@assets/datasource';
import { SortSettingsModel } from '@syncfusion/ej2-treegrid';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  isLoading = false;
  dataSource: GridModel[];
  tableColumns: SortSettingsModel = {};
  public sortSettings: SortSettingsModel;
  public pageSettings: number;

  constructor(private gridServiceService: GridServiceService) {
    /* These values should initializ in constructor else gives initialize error even if init in ngOnInit */
    this.dataSource = sampleData;
    this.gridServiceService.initializeSorting(sampleData);
    this.sortSettings = this.gridServiceService.sortSettings;
    this.pageSettings = 18;
  }

  ngOnInit() {}
}

// this.dataSource = sampleData.map(x => {
//   return  Rows.adapt(sampleData, GridModel.adapt(sampleData))
// });
//{ columns: [{ field: 'taskName', direction: 'Ascending' }, { field: 'taskID', direction: 'Descending' },  { field: 'startDate', direction: 'Ascending' }]  };
